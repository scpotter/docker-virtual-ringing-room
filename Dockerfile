FROM easypi/alpine-arm

RUN apk update && apk add vim python3 wget npm gcc git py3-pip python-dev musl-dev libffi-dev

RUN pip3 install --upgrade pip
RUN pip3 install flask
RUN npm install -g sass

RUN git clone https://github.com/lelandpaul/virtual-ringing-room.git
RUN cd virtual-ringing-room && pip3 install -r requirements.txt
RUN cd virtual-ringing-room && sass app/static/sass/:app/static/css/
